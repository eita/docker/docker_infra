```sh
              ================================================================
                             Help to docker-infra command line
              ================================================================

docker-infra provide commands to manage stack services that make up the web infrastructure.

Usage:  ./docker-infra.sh COMMAND [SUBCOMMANDS | OPTIONS]

Commands:
      swarm           Manage docker swarm on production.
        start
        stop
        restart

      proxy           Manage docker stack of proxy services on production.
        start
        stop
        restart

      monitor         Manage docker stack of monitor services on production.
        start
        stop
        restart

      networkreview   Create network 'proxy-reverse' if not exists.

      bootstrap       Call networkreview and start swarm, monitor and proxy.

      help | -h       This help text.
```
