#!/bin/bash

bootstrap() {
    proxy networkreview
    swarm start
    monitor start
    proxy start
}

swarm() {
    local cmdname=$1; shift
    if type "swarm_$cmdname" >/dev/null 2>&1; then
        "swarm_$cmdname" "$@"
    else
        command swarm "$cmdname" "$@"
    fi
}

swarm_start() {
    if [ $(docker info | grep Swarm | awk {'print $2'}) = "inactive" ]; then
        docker swarm init --advertise-addr=$(hostname -I | awk {'print $1'})
    else
        echo ""
        echo "Swarm has already been started."
    fi
}

swarm_stop() {
    docker swarm leave --force
}

swarm_restart() {
    swarm stop
    sleep 1
    swarm start
}

proxy() {
    local cmdname=$1; shift
    if type "proxy_$cmdname" >/dev/null 2>&1; then
        "proxy_$cmdname" "$@"
    else
        command proxy "$cmdname" "$@"
    fi
}

proxy_networkreview() {
    echo ""
    echo "network review..."
    network_name='reverse-proxy'
    # check if network exists
    if [[ -z $(docker network ls | grep $network_name) ]]; then
        echo ""
        echo "creating network $network_name"
        docker network create $network_name
    fi
}

proxy_start() {
    docker stack deploy \
        -c ./docker-compose.prod.nginx-proxy.yml \
        nginx-proxy
}

proxy_stop() {
    docker stack rm nginx-proxy
}

proxy_restart() {
    proxy stop
    proxy start
}

monitor() {
    local cmdname=$1; shift
    if type "monitor_$cmdname" >/dev/null 2>&1; then
        "monitor_$cmdname" "$@"
    else
        command monitor "$cmdname" "$@"
    fi
}

monitor_start() {

    # check if monitor is running
    if [[ ! -z $(docker ps | grep monitor_app | grep Up) ]]; then
        docker service update monitor_app
    else
        env_monitor_file='.envs/.env.monitor'
        if [ ! -z $(ls $env_monitor_file 2>/dev/null) ]; then
            source $env_monitor_file
        else
            echo ""
            echo "###############"
            echo "Monitor config"
            echo "###############"

            echo ""
            STACK_NAME=monitor
            echo -n "Enter stack name [$STACK_NAME]: ";
            read
            if [[ ! -z ${REPLY} ]]; then
                STACK_NAME=${REPLY}
            fi

            echo ""
            APP_PORT=888
            echo -n "Enter application port [$APP_PORT]: ";
            read
            if [[ ! -z ${REPLY} ]]; then
                APP_PORT=${REPLY}
            fi

            echo ""
            DB_VOLUME_DRIVER=local
            echo -n "Enter database volume driver [$DB_VOLUME_DRIVER]: ";
            read
            if [[ ! -z ${REPLY} ]]; then
                DB_VOLUME_DRIVER=${REPLY}
            fi

            echo ""
            ADMIN_USERNAME=admin
            echo -n "Enter admin username [$ADMIN_USERNAME]: ";
            read
            if [[ ! -z ${REPLY} ]]; then
                ADMIN_USERNAME=${REPLY}
            fi

            echo ""
            ADMIN_PASSWORD=$(date | md5sum | awk {'print $1'})
            echo -n "Enter admin password (min 8 characters long) [$ADMIN_PASSWORD]: ";
            read
            if [[ ! -z ${REPLY} ]]; then
                ADMIN_PASSWORD=${REPLY}
            fi

            echo ""
            echo "--------------------------------------------------------------------------------"
            echo ""
            echo "INFO: This setting will be stored in $env_monitor_file and can be changed at any time."
            echo ""
            echo -n "Type ENTER to continue"
            read

            mkdir .envs 2>/dev/null
            echo "STACK_NAME=$STACK_NAME" > $env_monitor_file
            echo "APP_PORT=$APP_PORT" >> $env_monitor_file
            echo "DB_VOLUME_DRIVER=$DB_VOLUME_DRIVER" >> $env_monitor_file
            echo "ADMIN_USERNAME=$ADMIN_USERNAME" >> $env_monitor_file
            echo "ADMIN_PASSWORD=$ADMIN_PASSWORD" >> $env_monitor_file
        fi

        docker run -it --rm \
            --name swarmpit-installer \
            --volume /var/run/docker.sock:/var/run/docker.sock \
            -e INTERACTIVE=0 \
            -e STACK_NAME=$STACK_NAME \
            -e APP_PORT=$APP_PORT \
            -e ADMIN_USERNAME=$ADMIN_USERNAME \
            -e ADMIN_PASSWORD=$ADMIN_PASSWORD \
            swarmpit/install:1.9

    fi
}

monitor_stop() {
    docker stack rm nginx-proxy
}

monitor_restart() {
    monitor stop
    monitor start
}

help() {
    cat help-text.md | awk -F'```' '{print $1}'
}

while getopts ":h" option; do
    case $option in
        h) # display Help
            help
            exit;;
    esac
done

# make sure we actually *did* get passed a valid function name
if declare -f "$1" >/dev/null 2>&1; then
    # invoke that function, passing arguments through
    "$@" # same as "$1" "$2" "$3" ... for full argument list
else
    echo "Function $1 not recognized" >&2
    help
fi
