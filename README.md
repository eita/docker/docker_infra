# docker_infra

Docker infrastructure to webserver. Contains:

* proxy-reverse: [jwilder/nginx-proxy](https://hub.docker.com/r/jwilder/nginx-proxy/)
* nginx-letsencrypt: [jrcs/letsencrypt-nginx-proxy-companion](https://hub.docker.com/r/jrcs/letsencrypt-nginx-proxy-companion)
* swarm monitor: [swarmpit](https://github.com/swarmpit/swarmpit)
